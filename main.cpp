#include <iostream>
#include "Dataset.h"
#include "RandomKernelInitializer.h"
#include "ConvLayer.h"
#include "MaxPoolLayer.h"

int main(int, char **) {
    auto ds = read_dataset("../train-images-idx3-ubyte", "../train-labels-idx1-ubyte", 10);
    if(!ds) {
        std::cout << "Error reading dataset!" << std::endl;
        exit(EXIT_FAILURE);
    }

    ds->getImages().at(9).show(std::cout);

    auto &input_image = ds->getImages().at(0);
    RandomKernelInitializer<float> kernel_init(0, 0.2);
    size_t nkernels = 8, kr = 3, kc = 3, pool_size = 2;
    ConvLayer<float> conv_layer(input_image.get_nrows(), input_image.get_ncols(), kr, kc, nkernels, kernel_init);
    MaxPoolLayer<float> maxpool_layer(conv_layer.output_rows(), conv_layer.output_cols(), pool_size);

    std::vector<Tensor<float>> conv_output_tensors;
    conv_output_tensors.reserve(nkernels);
    for (auto k : boost::irange(nkernels)) {
        (void) k;
        conv_output_tensors.emplace_back(conv_layer.output_rows(), conv_layer.output_cols());
    }

    std::vector<Tensor<float>> pool_output_tensors;
    pool_output_tensors.reserve(nkernels);
    for (auto k : boost::irange(nkernels)) {
        (void) k;
        pool_output_tensors.emplace_back(conv_layer.output_rows() / pool_size,
                                         conv_layer.output_cols() / pool_size);
    }

    conv_layer.forward(input_image, conv_output_tensors);
    maxpool_layer.forward(conv_output_tensors, pool_output_tensors);

    std::cout << std::endl;
    conv_output_tensors.at(0).show(std::cout);
    std::cout << std::endl;
    pool_output_tensors.at(0).show(std::cout);

    return 0;
}
