//
// Created by manjavi on 24/10/19.
//

#include <boost/range/irange.hpp>
#include "RandomKernelInitializer.h"

template <typename T>
RandomKernelInitializer<T>::RandomKernelInitializer(T mean, T std) : distribution(mean, std) {
    std::random_device urandom("/dev/urandom");
    this->number_generator.seed(urandom());
}

template <typename T>
Tensor<T> RandomKernelInitializer<T>::operator()(size_t nrows, size_t ncols) {
    auto weights = new float[nrows * ncols];
    for(auto i : boost::irange<size_t>(0, nrows * ncols)) {
        weights[i] = this->distribution(this->number_generator);
    }

    return Tensor(nrows, ncols, weights);
}

template class RandomKernelInitializer<float>;
