//
// Created by manjavi on 27/10/19.
//

#pragma once

#include <vector>
#include "Tensor.h"

template <typename T>
class MaxPoolLayer {
private:
    size_t input_rows, input_cols;
    size_t pool_size;

public:
    MaxPoolLayer(size_t input_rows, size_t input_cols, size_t pool_size);

    size_t output_rows() const;
    size_t output_cols() const;
    void forward(const std::vector<Tensor<T>> &input_tensors, std::vector<Tensor<T>> &output_tensors) const;
};
