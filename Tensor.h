//
// Created by javier on 1/10/19.
//

#pragma once


#include <cstdint>
#include <memory>
#include <optional>

template <typename T>
class Tensor {
private:
    size_t nrows, ncols;
    std::unique_ptr<T[]> w; // weights

public:
    // No copying or copy assignment allowed of this class or any derived class
    Tensor(Tensor<T> const &) = delete;
    Tensor &operator=(Tensor<T>&) = delete;
    Tensor(Tensor<T> &&);

    size_t get_nrows() const;
    size_t get_ncols() const;

    Tensor(size_t nrows, size_t ncols);
    Tensor(size_t nrows, size_t ncols, T fill);
    Tensor(size_t nrows, size_t ncols, T *weights);
    Tensor(const std::initializer_list<std::initializer_list<T>>);
    void show(std::ostream &out) const;
    T at(size_t r, size_t c) const;
    T& at(size_t r, size_t c);
    void convolve(Tensor<T> const &kernel, Tensor<T> &out) const;
};

/*template <typename T>
std::optional<Tensor<T>> read_tensor(std::istream &in);*/
