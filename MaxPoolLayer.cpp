//
// Created by manjavi on 27/10/19.
//

#include <sstream>
#include <boost/range/irange.hpp>
#include "MaxPoolLayer.h"

template<typename T>
MaxPoolLayer<T>::MaxPoolLayer(size_t input_rows, size_t input_cols, size_t pool_size)
    : input_rows(input_rows), input_cols(input_cols), pool_size(pool_size) {
    if (((input_rows % pool_size) != 0) or ((input_cols % pool_size) != 0)) {
        throw std::invalid_argument("Pooling size doesn't divide input dimensions evenly");
    }
}

template<typename T>
size_t MaxPoolLayer<T>::output_rows() const {
    return this->input_rows / this->pool_size;
}

template<typename T>
size_t MaxPoolLayer<T>::output_cols() const {
    return this->input_cols / this->pool_size;
}

template<typename T>
void MaxPoolLayer<T>::forward(const std::vector<Tensor<T>> &input_tensors,
                              std::vector<Tensor<T>> &output_tensors) const {
    if (input_tensors.size() != output_tensors.size()) {
        std::ostringstream s;
        s << "Expected output to be " << input_tensors.size() << " layers long, got" << output_tensors.size();
        throw std::logic_error(s.str());
    }

    auto outr = this->output_rows();
    auto outc = this->output_cols();
    for(size_t k=0; k < output_tensors.size(); ++k) {
        auto &input_tensor = input_tensors[k];
        auto &output_tensor = output_tensors[k];

        if ((output_tensors[k].get_nrows() != outr) or
            (output_tensors[k].get_ncols() != outc)) {
            std::ostringstream s;
            s << "Expected output tensor size to be (" << this->output_rows() << ", " << this->output_cols()
              << "), got (" << output_tensors[k].get_nrows() << ", " << output_tensors[k].get_ncols() << ")";
            throw std::logic_error(s.str());
        }

        for (auto i : boost::irange(outr)) {
            for (auto j : boost::irange(outc)) {
                size_t rowoff = i * pool_size;
                size_t coloff = j * pool_size;

                auto max = -std::numeric_limits<T>::max();
                for (auto pi : boost::irange(pool_size)) {
                    for (auto pj : boost::irange(pool_size)) {
                        auto val = input_tensor.at(rowoff+pi, coloff+pj);
                        if (val > max) max = val;
                    }
                }

                output_tensor.at(i, j) = max;
            }
        }
    }
}

template class MaxPoolLayer<float>;