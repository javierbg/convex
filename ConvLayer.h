//
// Created by javier on 24/10/19.
//

#pragma once


#include <vector>
#include "Tensor.h"
#include "KernelInitializer.h"

template <typename T>
class ConvLayer {
private:
    size_t input_rows, input_cols;
    size_t kernel_rows, kernel_cols;
    std::vector<Tensor<T>> kernels;

public:
    ConvLayer(size_t input_rows, size_t input_cols, size_t nrows, size_t ncols, size_t n_kernels,
              KernelInitializer<T> &ki);

    size_t output_rows() const;
    size_t output_cols() const;
    void forward(const Tensor<T> &input_tensor, std::vector<Tensor<T>> &output_tensors) const;
};



