//
// Created by javier on 24/10/19.
//

#pragma once


#include "Tensor.h"

template <typename T>
class KernelInitializer {
public:
    virtual Tensor<T> operator()(size_t nrows, size_t ncols) = 0;
};



