//
// Created by javier on 1/10/19.
//

#include "Tensor.h"

#include <iostream>
#include <cstring>
#include <optional>
#include <sstream>
#include <stdexcept>
#include <boost/range/irange.hpp>

template <typename T>
Tensor<T>::Tensor(size_t nrows, size_t ncols, T *weights) {
    this->nrows = nrows;
    this->ncols = ncols;
    this->w = std::unique_ptr<T[]>(weights);
}

template <typename T>
T Tensor<T>::at(size_t r, size_t c) const {
    if ((r > this->nrows) or (c > this->ncols)) {
        std::ostringstream s;
        s << "Invalid index (" << r << ", " << c << ") for tensor with dimensions (" << this->nrows << ", " << this->ncols;
        throw std::out_of_range(s.str());
    }

    return this->w[c + r * this->ncols];
}

template <typename T>
T& Tensor<T>::at(size_t r, size_t c) {
    if ((r > this->nrows) or (c > this->ncols)) {
        std::ostringstream s;
        s << "Invalid index (" << r << ", " << c << ") for tensor with dimensions "
          << "(" << this->nrows << ", " << this->ncols << ")";
        throw std::out_of_range(s.str());
    }

    return this->w[c + r * this->ncols];
}

template <typename T>
void Tensor<T>::convolve(Tensor<T> const &kernel, Tensor<T> &out) const {
    auto krows = kernel.get_nrows();
    auto kcols = kernel.get_ncols();
    auto expected_rows = this->nrows - krows + 1;
    auto expected_cols = this->ncols - kcols + 1;
    if ((out.get_nrows() != expected_rows) or (out.get_ncols() != expected_cols)) {
        std::ostringstream s;
        s << "Invalid output tensor with dimensions (" << out.get_nrows() << ", " << out.get_ncols() << ") "
          << "for convolution of (" << this->nrows << ", " << this->ncols << ") "
          << "with  kernel (" << kernel.get_nrows() << ", " << kernel.get_ncols() << ")";
        throw std::logic_error(s.str());
    }

    for(auto roff : boost::irange<size_t>(0, expected_rows)) {
        for(auto coff : boost::irange<size_t>(0, expected_cols)) {
            out.at(roff, coff) = 0;
            for(auto rker : boost::irange<size_t>(krows)) {
                for(auto cker : boost::irange<size_t>(kcols)) {
                    out.at(roff, coff) += kernel.at(rker, cker) * this->at(roff + rker, coff + cker);
                }
            }
        }
    }
}

template <typename T>
size_t Tensor<T>::get_nrows() const {
    return this->nrows;
}

template <typename T>
size_t Tensor<T>::get_ncols() const {
    return this->ncols;
}

template <typename T>
void Tensor<T>::show(std::ostream &out) const {
    out << "(" << this->nrows << ", " << this->ncols << ")" << std::endl;

    for (size_t r = 0; r < this->nrows; r++) {
        bool first_column = true;
        for (size_t c = 0; c < this->ncols; c++) {
            if (!first_column) out << ", ";
            out << this->w[c + r * ncols];
            first_column = false;
        }
        out << std::endl;
    }
}

template <typename T>
Tensor<T>::Tensor(size_t nrows, size_t ncols) :
        nrows(nrows),
        ncols(ncols),
        w(new T[nrows * ncols]) {
}

template <typename T>
Tensor<T>::Tensor(size_t nrows, size_t ncols, T fill) :
    nrows(nrows),
    ncols(ncols),
    w(new T[nrows * ncols]) {
    for(auto i : boost::irange(nrows * ncols)) {
        w[i] = fill;
    }
}

template <typename T>
Tensor<T>::Tensor(const std::initializer_list<std::initializer_list<T>> init) {
    this->nrows = init.size();

    if (this->nrows == 0) {
        this->ncols = 0;
        return;
    }

    this->ncols = init.begin()->size();
    auto arr = new T[this->nrows * this->ncols];

    size_t i = 0;
    for (const auto row : init) {
        if (row.size() != this->ncols) {
            throw std::invalid_argument("All rows of initializer list must be the same length");
        }
        for (const auto val : row) {
            arr[i++] = val;
        }
    }
    this->w = std::unique_ptr<T[]>(arr);
}

template <typename T>
Tensor<T>::Tensor(Tensor<T> &&other) {
    std::swap(this->w, other.w);
    std::swap(this->nrows, other.nrows);
    std::swap(this->ncols, other.ncols);
}

template class Tensor<float>;
