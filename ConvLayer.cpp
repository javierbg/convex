//
// Created by javier on 24/10/19.
//

#include <boost/range/irange.hpp>
#include <boost/range/combine.hpp>
#include <sstream>
#include "ConvLayer.h"

template <typename T>
ConvLayer<T>::ConvLayer(size_t input_rows, size_t input_cols, size_t nrows, size_t ncols,
                        size_t n_kernels, KernelInitializer<T> &ki)
    : input_rows(input_rows),
      input_cols(input_cols),
      kernel_rows(nrows),
      kernel_cols(ncols) {

    this->kernels.reserve(n_kernels);
    for (auto i : boost::irange(n_kernels)) {
        (void) i; // unused
        this->kernels.push_back(ki(nrows, ncols));
    }
}

template<typename T>
size_t ConvLayer<T>::output_rows() const {
    return this->input_rows - this->kernel_rows + 1;
}

template<typename T>
size_t ConvLayer<T>::output_cols() const {
    return this->input_cols - this->kernel_cols + 1;
}

template<typename T>
void ConvLayer<T>::forward(const Tensor<T> &input_tensor, std::vector<Tensor<T>> &output_tensors) const {
    if (output_tensors.size() != this->kernels.size()) {
        std::ostringstream s;
        s << "Expected output to be " << this->kernels.size() << " kernels long, got" << output_tensors.size();
        throw std::logic_error(s.str());
    }

    auto outr = this->output_rows();
    auto outc = this->output_cols();
    for(size_t k=0; k < output_tensors.size(); ++k) {
        if ((output_tensors[k].get_nrows() != outr) or
            (output_tensors[k].get_ncols() != outc)) {
            std::ostringstream s;
            s << "Expected output tensor size to be (" << this->output_rows() << ", " << this->output_cols()
              << "), got (" << output_tensors[k].get_nrows() << ", " << output_tensors[k].get_ncols() << ")";
            throw std::logic_error(s.str());
        }

        input_tensor.convolve(this->kernels[k], output_tensors[k]);
    }
}

template class ConvLayer<float>;
