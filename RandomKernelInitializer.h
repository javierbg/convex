//
// Created by manjavi on 24/10/19.
//

#pragma once


#include <random>
#include "KernelInitializer.h"

template <typename T>
class RandomKernelInitializer : public KernelInitializer<T>{
    using rng = std::mt19937;
    using dist = std::normal_distribution<T>;
private:
    rng number_generator;
    dist distribution;

public:
    RandomKernelInitializer(T mean, T std);
    Tensor<T> operator()(size_t nrows, size_t ncols) override;
};



