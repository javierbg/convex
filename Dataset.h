//
// Created by manjavi on 25/10/19.
//

#pragma once


#include <optional>
#include <vector>
#include <sstream>
#include <fstream>
#include <algorithm>
#include <boost/range/irange.hpp>
#include "Tensor.h"


template <typename Data, typename Label>
class Dataset {
private:
    std::vector<Tensor<Data>> images;
    std::vector<Label> labels;
public:
    const std::vector<Tensor<Data>> &getImages() const {
        return images;
    }

    const std::vector<Label> &getLabels() const {
        return labels;
    }

public:
    Dataset(std::vector<Tensor<Data>> &images, std::vector<Label> &labels) {
        if (images.size() != labels.size()) {
            std::ostringstream s;
            s << "Can't construct dataset due to incompatible dimensions: " << images.size() << " images and "
              << labels.size() << " labels were given.";
            throw std::invalid_argument(s.str());
        }
        std::swap(this->images, images);
        std::swap(this->labels, labels);
    }
};

template <class T>
std::optional<T> read_binary_value(std::istream &in) {
    T val;
    in.read(reinterpret_cast<char *>(&val), sizeof(T));
    if (in.fail()) return {};
    return val;
}

std::optional<std::vector<Tensor<float>>> read_images(const std::string &images_path, size_t max_samples) {
    std::ifstream in(images_path, std::ios::binary);

    // Read IDX magic number for a set of 2D images as unsigned 8 bit values
    uint32_t expected_magic_number = 0x00000803;

    // It's necessary to correct for endianness
    auto magic_number = read_binary_value<int32_t>(in);
    if ((not magic_number.has_value()) or (htobe32(*magic_number) != expected_magic_number)) {
        return {};
    }

    auto nimages = read_binary_value<uint32_t>(in);
    auto nrows = read_binary_value<uint32_t>(in);
    auto ncols = read_binary_value<uint32_t>(in);

    if ((!nimages) or (!nrows) or (!ncols)) return {};

    // Again, correct for endianness
    auto ni = size_t(htobe32(nimages.value()));
    auto nr = htobe32(nrows.value());
    auto nc = htobe32(ncols.value());

    std::vector<Tensor<float>> images;
    images.reserve(ni);

    for(auto i : boost::irange(std::min(ni, max_samples))) {
        (void) i; // unused
        auto input_tensor = new float[nr * nc];
        for(auto r : boost::irange(nr)) {
            for (auto c: boost::irange(nc)) {
                auto val = read_binary_value<uint8_t>(in);
                if (!val) return {};
                auto floatval = float(*val);
                floatval = (floatval / 255.0) - 0.5; // Map from [0, 255] to [-0.5, 0.5]
                input_tensor[c + r * nc] = floatval;
            }
        }
        images.emplace_back(nr, nc, input_tensor);
    }

    return images;
}

std::optional<std::vector<int>> read_labels(const std::string &images_path, size_t max_samples) {
    std::ifstream in(images_path, std::ios::binary);

    // Read IDX magic number for a set of labels as unsigned 8 bit values
    uint32_t expected_magic_number = 0x00000801;

    // It's necessary to correct for endianness
    auto magic_number = read_binary_value<int32_t>(in);
    if ((not magic_number.has_value()) or (htobe32(*magic_number) != expected_magic_number)) {
        return {};
    }

    auto nlabels = read_binary_value<uint32_t>(in);

    if (!nlabels) return {};

    // Again, correct for endianness
    auto nl = size_t(htobe32(*nlabels));

    std::vector<int> labels;
    labels.reserve(nl);

    for(auto i : boost::irange(std::min(nl, max_samples))) {
        (void) i; // unused
        auto val = read_binary_value<uint8_t>(in);
        if (!val) return {};
        labels.push_back(int(*val));
    }

    return labels;
}

std::optional<Dataset<float, int>> read_dataset(const std::string &images_path, const std::string &labels_path,
                                                size_t max_samples = std::numeric_limits<size_t>::max()) {
    auto images = read_images(images_path, max_samples);
    auto labels = read_labels(labels_path, max_samples);

    if ((not images.has_value()) or (not labels.has_value())) {
        return {};
    }

    return Dataset<float, int>(*images, *labels);
}
